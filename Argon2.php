<?php

namespace Argon2;

use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\InstallContext;

class Argon2 extends Plugin
{

    public function install(InstallContext $installContext)
    {
        if (!$this->checkIfPHPVersionIsMet()) {
            throw new \Exception('Plugin require PHP 7.2 or higher');
        }

        return true;
    }

    private function checkIfPHPVersionIsMet(): bool
    {
        return version_compare(phpversion(), '7.2', '>=');
    }

}