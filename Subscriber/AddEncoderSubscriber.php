<?php

namespace Argon2\Subscriber;

use Enlight\Event\SubscriberInterface;
use Enlight_Event_EventArgs;
use Argon2\Components\Argon2Encoder;

class AddEncoderSubscriber implements SubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'Shopware_Components_Password_Manager_AddEncoder' => 'onAddEncoder'
        ];
    }

    /**
     * Add the encoder to the internal encoder collection
     *
     * @param Enlight_Event_EventArgs $args
     * @return array
     */
    public function onAddEncoder(Enlight_Event_EventArgs $args): array
    {
        $passwordHashHandler = $args->getReturn();

        $passwordHashHandler[] = new Argon2Encoder();

        return $passwordHashHandler;
    }
}