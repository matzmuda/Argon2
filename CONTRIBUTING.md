Shopware 5 - Das Plugin registriert zusätzliche Encoder (Argon 2).

Min Shopware version: 5.3
Min PHP version:  7.2

Author:
Mateusz Zmuda - Shopware Developer
<a href="http://mzmuda.de">mzmuda.de</a>
