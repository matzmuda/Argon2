<?php

namespace Argon2\Components;

use Shopware\Components\Password\Encoder\PasswordEncoderInterface;

class Argon2Encoder implements PasswordEncoderInterface
{
    /**
     * @return string
     */
    public function getName()
    {
        return 'Argon2';
    }

    /**
     * @param  string $password
     * @return string
     */
    public function encodePassword($password): string
    {
        return password_hash($password, PASSWORD_ARGON2I);
    }

    /**
     * @param  string $password
     * @param  string $hash
     * @return bool
     */
    public function isPasswordValid($password, $hash): bool
    {
        return password_verify($password, $hash);
    }

    /**
     * @param  string $hash
     * @return bool
     */
    public function isReencodeNeeded($hash): bool
    {
        return false;
    }
}
