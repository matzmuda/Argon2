<?php

namespace Argon2\tests;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class Argon2EncoderTest extends Shopware\Components\Test\Plugin\TestCase
{
    public function testHashEncoder()
    {
        $encoder = new Argon2\Components\Argon2Encoder();
        $hash = $encoder->encodePassword('password');

        $this->assertEquals($hash, '$argon2i$v=19$m=1024,t=2,p=2$eEpDS1dlQnFmTC5jZ09NQQ$GCfovty/AfR4Da8DtF3mT0sWMquH5D3FYdeTg/CgWo4');
    }

    public function testIsValid()
    {
        $encoder = new Argon2\Components\Argon2Encoder();
        $isValid = $encoder->isPasswordValid('password', '$argon2i$v=19$m=1024,t=2,p=2$eEpDS1dlQnFmTC5jZ09NQQ$GCfovty/AfR4Da8DtF3mT0sWMquH5D3FYdeTg/CgWo4');

        $this->assertTrue($isValid);
    }

    public function testInvalidPassword()
    {
        $encoder = new Argon2\Components\Argon2Encoder();
        $isValid = $encoder->isPasswordValid('password', '$argon2i$v=19$m=1024,t=2,p=2$SomeRandomHash');

        $this->assertFalse($isValid);
    }
}
