<?php

require_once __DIR__ . '/../../../../autoload.php';

$enlightLoader = new Enlight_Loader();

$enlightLoader->registerNamespace(
    'Argon2',
    __DIR__ . '/../'
);
